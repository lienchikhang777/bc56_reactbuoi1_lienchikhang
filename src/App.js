import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import Body from './Components/Body';
import Footer from './Components/Footer';

function App() {
  return (
    <div className="App">
      <header>
        <div className="container">
          <Header />
        </div>
      </header>
      <div className="container mt-5">
        <Body />
      </div>
      <footer className="p-4">
        <Footer />
      </footer>
    </div>
  );
}

export default App;
