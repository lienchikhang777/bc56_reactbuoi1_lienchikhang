import React, { Component } from 'react'
import '../Item.css'

export default class extends Component {
    render() {
        return (
            <div className="col-xl-4 col-lg-6 col-md-12">
                <div className="item position-relative rounded p-4 my-4">
                    <div className="item__icon bg-primary rounded p-2 text-white position-absolute">
                        <i class="fa-solid fa-cloud-arrow-down"></i>
                    </div>
                    <div className="item__info p-2">
                        <h2 className="item__title font-weight-bold">Fresh new layout</h2>
                        <p className="item__desc">With Bootstrap 5, we've created a fresh new layout for this template!</p>
                    </div>
                </div>
            </div>
        )
    }
}
