import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div className="p-4 text-white font-weight-bold">Copyright © Your Website 2023</div>
        )
    }
}

