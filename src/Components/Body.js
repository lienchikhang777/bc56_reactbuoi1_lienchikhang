import React, { Component } from 'react'
import Banner from './Banner'
import Item from './Item'

export default class Body extends Component {
    render() {
        return (
            <div>
                <div className="myBanner">
                    <Banner />
                </div>
                <div className="itemList p-sm-5 p-1">
                    <div className="row">
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                    </div>
                </div>
            </div>

        )
    }
}
