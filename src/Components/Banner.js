import React, { Component } from 'react'
import '../Banner.css'

export default class Banner extends Component {
    render() {
        return (
            <div className="banner p-5 rounded">
                <div className="m-5">
                    <h1 className="banner__title display-5 font-weight-bold">A warm welcome!</h1>
                    <p className="banner__desc display-5">Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                    <button className="banner__btn btn btn-primary rounded font-weight-bold">Call to action</button>
                </div>
            </div>
        )
    }
}
